package algoritmen;

import static org.junit.jupiter.api.Assertions.*;

import java.util.EmptyStackException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestStack {

	Stack<String> stack;
	
	@BeforeEach
	void setUp() throws Exception {
		stack = new StackAdapter<String>();
	}

	@Test
	void testTop() {
		//TODO deal with the nullpointerexception
	}

	@Test
	void testPushElements() {
		stack.push("Hallo");
		assertEquals("Hallo", stack.top());
		assertEquals(1, stack.size());
		assertEquals("Hallo", stack.pop());
		assertEquals(0, stack.size());
	}

	@Test
	void testSize() {
		assertEquals(0, stack.size());
	}

	@Test
	void testIsEmpty() {
		assertTrue(stack.isEmpty());
		assertThrows(EmptyStackException.class, () ->{
			stack.top();
		});
	}

}
