package algoritmen;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestLinkedList {

	LinkedList<String> ll2; 
	
	@BeforeEach
	void setUp(){
		ll2 = new LinkedList<String>("hallo");
	}

	@Test
	void testGetSize() {
		LinkedList<String> ll = new LinkedList<String>();
		assertEquals(0, ll.getSize());
		assertEquals(1, ll2.getSize());
	}

	
	@Test
	void testGetHead() {
		assertEquals("hallo" ,ll2.getHead());
	}
	
	@Test
	void testPrepend() {
		ll2.prepend("wereld");
		assertEquals("wereld" ,ll2.getHead());
		assertEquals(2 ,ll2.getSize());
	}
	
	@Test
	void testGetTail() {
		ll2.prepend("wereld");
		LinkedList<String> ll1 = ll2.getTail();
		assertEquals("hallo" ,ll1.getHead());
		assertEquals(1,ll1.getSize());
	}
	
	@Test
	void testRemove() {
		ll2.prepend("wereld");
		ll2.remove("hallo");
		assertEquals(1, ll2.getSize());
		assertEquals("wereld", ll2.getHead());
		assertEquals(0, ll2.getTail().getSize());
		ll2.remove("wereld");
		assertEquals(0, ll2.getSize());
	}
	
	@Test
	void testIterator() {
		ll2.prepend("hallo");
		ll2.prepend("hallo");
		int counter = 0;
		for(String s : ll2) {
			assertEquals("hallo", s);
			counter++;
		}
		assertEquals(3, counter);
	}
}
