package algoritmen;

import java.util.Iterator;

/**
 * 
 * Singly linked list implementation
 *
 * @param <E>
 */
public class LinkedList<E> implements Iterable<E>{
	private int size;
	private Node head;
	
	/**
	 * Constructs an empty linked list
	 */
	public LinkedList() {
		size = 0;
		head = null;
	}
	
	/**
	 * Constructs a linked list with a single element
	 * 
	 * @param element of the linked list
	 */
	public LinkedList(E element) {
		size = 1;
		head = new Node(element);
	}
	
	/**
	 * Constructs a linked list from a head node
	 * @param node head
	 * @param size number of nodes
	 */
	private LinkedList(Node node, int size) {
		head = node;
		this.size = size;
	}
	
	/**
	 * 
	 * @return the number of elements in the linked list
	 */
	public int getSize() {
		return size;
	}
	
	/**
	 * 
	 * @return true if the linked list contains no elements
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Adds an element to the front of the linked list
	 * 
	 * @param element
	 */
	public void prepend(E element) {
		Node temp = new Node(element, head);
		head = temp;
		size++;
	}
	
	/**
	 * 
	 * @return the element at the front of the linked list
	 */
	public E getHead() {
		return head.getElement();
	}
	
	/**
	 * 
	 * @return the linked list without the first element
	 */
	public LinkedList<E> getTail(){
		return new LinkedList<E>(head.getNext(), size-1);
	}
	
	/**
	 * Removes an element from the linked list
	 * Precondition: the element must be present in the linked list
	 * 
	 * @param element
	 */
	public void remove(E element) {
		Node toBeRemoved;
		if(head.getElement().equals(element)) {
			toBeRemoved = head;
			head = toBeRemoved.getNext();
			
		}
		else {
			Node before = searchNodeBefore(element, head);
			toBeRemoved = before.getNext();
			before.setNext(toBeRemoved.getNext());			
		}
		toBeRemoved.setNext(null);
		size--;
	}
	
	/**
	 * Searches the node before the node of a given element
	 * @param element to find
	 * @param cursor 
	 * @return the node before the node of a given element
	 */
	private Node searchNodeBefore(E element, Node cursor) {
		if(cursor == null || cursor.getNext() == null) return null;
		if(cursor.getNext().getElement().equals(element)) return cursor;
		return searchNodeBefore(element, cursor.getNext());
	}
	
	/**
	 * This is a node containing an element used in a singly linked list
	 * 
	 *
	 * @param <E> type parameter
	 */
	private class Node {
		private E element;
		private Node next;
		
		/***
		 * Creates a node with a single element, next referring to null
		 * 
		 * @param element
		 */
		public Node(E element) {
			this(element, null);
		}
		
		/**
		 * Creates a node with a single element, next referring to the next node
		 * @param element
		 * @param next
		 */
		public Node(E element, Node next) {
			this.element = element;
			this.next = next;
		}
		
		/**
		 * Getter for the element
		 *  
		 * @return element
		 */
		public E getElement() {
			return element;
		}
		
		/**
		 * Getter for the next node
		 * 
		 * @return next node
		 */
		public Node getNext(){
			return next;
		}
		
		/**
		 * Setter for the next node
		 *  
		 * @param next
		 */
		public void setNext(Node next) {
			this.next = next;
		}

	}

	@Override
	public Iterator<E> iterator() {
		return new LinkedListIterator();
	}
	
	private class LinkedListIterator implements Iterator<E>{
		private Node cursor = head;
		@Override
		public boolean hasNext() {
			return cursor != null;
		}

		@Override
		public E next() {
			E element = cursor.getElement();
			cursor = cursor.getNext();
			return element;
		}
	
	}

}