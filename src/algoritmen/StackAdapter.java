/**
 * 
 */
package algoritmen;

import java.util.EmptyStackException;

/**
 * @author Piet
 *
 */
public class StackAdapter<E> implements Stack<E> {

	private LinkedList<E> list;
	
	public StackAdapter() {
		list = new LinkedList<E>();
	}
	
	/* (non-Javadoc)
	 * @see algoritmen.Stack#push(java.lang.Object)
	 */
	@Override
	public void push(E element) {
		list.prepend(element);
	}

	/* (non-Javadoc)
	 * @see algoritmen.Stack#top()
	 */
	@Override
	public E top() {
		if(isEmpty()) throw new EmptyStackException();
		return list.getHead();
	}

	/* (non-Javadoc)
	 * @see algoritmen.Stack#pop()
	 */
	@Override
	public E pop() {
		E element = top();
		list = list.getTail();
		return element;
	}

	/* (non-Javadoc)
	 * @see algoritmen.Stack#size()
	 */
	@Override
	public int size() {
		return list.getSize();
	}

	/* (non-Javadoc)
	 * @see algoritmen.Stack#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

}
