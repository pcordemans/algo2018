package algoritmen;

import java.util.Iterator;

public class DynamicArray<E> implements IndexList<E> {
	private E[] rij;
	private int size;
	
	@SuppressWarnings("unchecked")
	public DynamicArray() {
		rij = (E[]) new Object[2];
		size = 0;
	}
	
	@Override
	public Iterator<E> iterator() {
		return new DynamicIterator();
	}

	private class DynamicIterator implements Iterator<E>{
		private int index = 0;
		@Override
		public boolean hasNext() {
			return index < size;
		}

		@Override
		public E next() {
			E element = rij[index];
			index++;
			return element;
		}
		
	}
	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public int size() {
		
		return size;
	}

	@Override
	public E set(int index, E element) {
		if(index >= rij.length) doubleCapacity(nTimesPow2(index, rij.length));
		E old = get(index);
		rij[index] = element;
		if(old == null) size++;
		return old;
	}
	/**
	 * 
	 * @param wanted
	 * @param initial
	 * @return wanted <= result * initial, where result = 2^n 
	 */
	private int nTimesPow2(int wanted, int initial) {
		int result = 1;
		while(wanted >= initial * result * 2) {
			result++;
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	private void doubleCapacity(int times) {
		E[] temp = (E[]) new Object[rij.length * 2 * times];
		for(int i = 0; i < rij.length; i++) {
			temp[i] = rij[i];
		}
		rij = temp;
	}

	@Override
	public E get(int index) {
		return rij[index];
	}

	@Override
	public E remove(int index) {
		E element = get(index);
		rij[index] = null;
		if(element != null)size--;
		return element;
	}

}
